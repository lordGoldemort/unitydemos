﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using UnityEngine.Networking;

public class DictionaryPanel : MonoBehaviour
{
    public TMP_Text SelectedWordText;
    public TMP_Text DefinitionText;
    public Button DoneButton;
    public Image BusySpinner;

    void Start()
    {
        BusySpinner.gameObject.SetActive(false);
    }
    
    public void Lookup(string word)
    {
        SelectedWordText.text = word;
        StartCoroutine(lookupInner(word));
    }

    private IEnumerator lookupInner(string word)
    {
        DefinitionText.text = "Looking up OED...";
        BusySpinner.gameObject.SetActive(true);
        
        var url = "https://od-api.oxforddictionaries.com:443/api/v2/entries/en/" + word.ToLower();
        var www = new UnityWebRequest(url) {
            method = UnityWebRequest.kHttpVerbGET,
            useHttpContinue = false,
            chunkedTransfer = false,
            timeout = 60,
            downloadHandler = new DownloadHandlerBuffer()
        };
        www.SetRequestHeader("app_id", "fcb438ba");
        www.SetRequestHeader("app_key", "ec37d5ce055dec862f08b74dd25d18cf");
        yield return www.SendWebRequest();
        BusySpinner.gameObject.SetActive(false);
        var definition = DemoUtils.ParseResponse(www.downloadHandler.text);
        DefinitionText.text = definition;
    }

    private static readonly Dictionary<string, string> s_headers = new Dictionary<string, string>()
    {
        {"app_id", "fcb438ba" },
        {"app_key", "ec37d5ce055dec862f08b74dd25d18cf" }
    };
}
