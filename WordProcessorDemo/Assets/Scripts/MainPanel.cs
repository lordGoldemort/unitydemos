﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainPanel : MonoBehaviour
{
    public TMP_InputField UserText;
    public Button DictionaryButton;
    public Button CopyButton;
    public Button CutButton;
    public Button PasteButton;
    public TMP_Text StatusText;
    public float StatusFadeRate = 0.25f;
    public ObjectPool TilePool;
    
    public string SelectedText { get; private set; }

    private void Start()
    {
        DictionaryButton.interactable = false;
        CopyButton.interactable = false;
        CutButton.interactable = false;
        UserText.onTextSelection.AddListener(OnTextSelected);
        SetStatusText("Welcome to Holoword, where your words are never hollow...");      
    }

    public void SetStatusText(string status)
    {
        StopCoroutine(nameof(fadeStatus));
        _alpha = 1.0f;
        StatusText.text = status;
        updateStatusColor();
        StartCoroutine(nameof(fadeStatus));
    }

    public void CutSelection()
    {
        if (_selectionEnd > _selectionStart) {
            var before = UserText.text.Substring(0, _selectionStart);
            var after = UserText.text.Substring(_selectionEnd);
            UserText.text = before + after;
            _selectionEnd = _selectionStart = 0;
            CutButton.interactable = false;
        }
    }

    private void OnTextSelected(string text, int start, int end)
    {
        if (start < end) {
            var selection = text.Substring(start, end - start);
            var valid = (!string.IsNullOrEmpty(selection) && !string.IsNullOrWhiteSpace(selection));
            DictionaryButton.interactable = valid;
            CopyButton.interactable = valid;
            CutButton.interactable = valid;
            SelectedText = selection;
            _selectionStart = start;
            _selectionEnd = end;
        }
    }

    public void OnClearClicked()
    {
        // 2. Create character tiles for the first N letters.
        var numTiles = Mathf.Min(TilePool.PoolSize, UserText.text.Length);
        if (numTiles == 0) {
            // Empty string. Nowt to do.
            return;
        }

        var meshTransform = UserText.transform;
        var textComponent = UserText.textComponent; 
        for(var i = 0; i < numTiles; ++i) {
            var symbol = UserText.text[i];
            if (char.IsWhiteSpace(symbol)) {
                continue;
            }

            var charInfo = textComponent.textInfo.characterInfo[i];
            var isCharacterVisible = (i < textComponent.maxVisibleCharacters) && 
                                     charInfo.lineNumber < textComponent.maxVisibleLines && 
                                     (textComponent.overflowMode != TextOverflowModes.Page || charInfo.pageNumber + 1 == textComponent.pageToDisplay);
            if (!isCharacterVisible) {
                continue;
            }   
            
            var botLeft = meshTransform.TransformPoint(charInfo.bottomLeft.x, charInfo.bottomLeft.y, 0);
            var topRight = meshTransform.TransformPoint(charInfo.topRight.x, charInfo.topRight.y, 0);
            var centre = new Vector3((botLeft.x + topRight.x)/2, (botLeft.y + topRight.y)/2, botLeft.z);
            var tile = TilePool.Allocate<CharacterTileController>();
            tile.transform.position = centre;
            tile.SetSymbol(symbol);
            tile.gameObject.SetActive(true);
        }

        UserText.text = string.Empty;
    }
   
    private IEnumerator fadeStatus()
    {
        while (_alpha > 0.0f) {
            _alpha -= StatusFadeRate * Time.deltaTime;
            if (_alpha < 0.0f) {
                _alpha = 0.0f;
            }
            updateStatusColor();
            yield return null;
        }
    }
    
    private void updateStatusColor()
    {
        var color = StatusText.color;
        color.a = _alpha;
        StatusText.color = color;
    }
    
    private float _alpha = 1f;
    private int _selectionStart;
    private int _selectionEnd;
}
