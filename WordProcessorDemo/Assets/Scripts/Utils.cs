// /**
//  * \file
//  * \brief
//  *
//  * (c) Vivid-Q Ltd 2018
//  */

using System;
using System.Collections.Generic;
using UnityEngine;

// ReSharper disable InconsistentNaming

[Serializable]
public class Domain
{
    public string id ;
    public string text ;
}

[Serializable]
public class Example
{
    public string text ;
}

[Serializable]
public class Note
{
    public string text ;
    public string type ;
}

[Serializable]
public class ThesaurusLink
{
    public string entry_id ;
    public string sense_id ;
}

[Serializable]
public class Subsense
{
    public List<string> definitions ;
    public List<Domain> domains ;
    public List<Example> examples ;
    public string id ;
    public List<Note> notes ;
    public List<string> shortDefinitions ;
    public List<ThesaurusLink> thesaurusLinks ;
}

[Serializable]
public class Sense : Subsense
{
    public List<Subsense> subsenses ;
}

[Serializable]
public class Entry
{
    public List<string> etymologies ;
    public List<Sense> senses ;
}

[Serializable]
public class Pronunciation
{
    public string audioFile ;
    public List<string> dialects ;
    public string phoneticNotation ;
    public string phoneticSpelling ;
}


[Serializable]
public class LexicalCategory
{
    public string id ;
    public string text ;
}

[Serializable]
public class LexicalEntry
{
    public List<Entry> entries;
    public string language ;
    public LexicalCategory lexicalCategory ;
    public List<Pronunciation> pronunciations ;
}

[Serializable]
public class Result
{
    public string id;
    public string language;
    public List<LexicalEntry> lexicalEntries;
    public string type;
    public string word;
}

[Serializable]
public class Metadata
{
    public string operation;
    public string provider;
    public string schema;
}

[Serializable]
public class Response
{
    public string id;
    public Metadata metadata;
    public List<Result> results;
    public string word;
}

// ReSharper restore InconsistentNaming

public static class DemoUtils
{
    public static string ParseResponse(string responseJson)
    {
        try {
            var response = JsonUtility.FromJson<Response>(responseJson);
            return response.results[0].lexicalEntries[0].entries[0].senses[0].shortDefinitions[0];            
        }
        catch(Exception ex) {
            Debug.Log("Exception: " + ex.Message);
            return "That's probably not a cromulant word";
        }
    }
}
