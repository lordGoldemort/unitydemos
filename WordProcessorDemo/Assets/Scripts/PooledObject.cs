using UnityEngine;

public class PooledObject : MonoBehaviour
{
    public ObjectPool Pool { get; set; }

    public void Dispose()
    {
        Pool.Free(gameObject);
    }
}
