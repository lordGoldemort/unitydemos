﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlowController : MonoBehaviour
{
    public float StartAlpha = 0.0f;
    public float EndAlpha = 1.0f;
    public Image Glow;
    
    private void Start()
    {
        _time = 0.0f;
        updateAlpha();
    }

    private void Update()
    {
        _time += Time.deltaTime;
        updateAlpha();
    }

    private void updateAlpha()
    {
        var alpha = StartAlpha + Mathf.Sin(_time) * (EndAlpha - StartAlpha);
        var color = Glow.color;
        color.a = alpha;
        Glow.color = color;
    }
    
    private float _time;
}
