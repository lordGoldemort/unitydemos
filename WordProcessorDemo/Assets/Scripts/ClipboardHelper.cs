﻿using UnityEngine;
using System;
using System.Reflection;
 
public static class ClipboardHelper
{       
    public static string ClipBoard
    {
        get => GUIUtility.systemCopyBuffer;
        set => GUIUtility.systemCopyBuffer = value;
    }
}