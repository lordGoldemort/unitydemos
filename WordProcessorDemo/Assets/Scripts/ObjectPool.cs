using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public GameObject Prototype;
    public int PoolSize;
    
    void Start()
    {
        for (var i = 0; i < PoolSize; ++i) {
            var item = Instantiate(Prototype, transform);
            item.SetActive(false);
            item.GetComponent<PooledObject>().Pool = this;
            _pool.Enqueue(item);
        }
    }

    public T Allocate<T>() where T : PooledObject
    {
        return (_pool.Count > 0) ? _pool.Dequeue().GetComponent<T>() : null;
    }

    public void Free(GameObject item)
    {
        item.SetActive(false);
        _pool.Enqueue(item);    
    }
    
    private readonly Queue<GameObject> _pool = new Queue<GameObject>();
}
