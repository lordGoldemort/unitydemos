﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public float RotateSpeed = 200f;

    private void Start()
    {
        _rectComponent = GetComponent<RectTransform>();
    }

    private void Update()
    {
        _rectComponent.Rotate(0f, 0f, RotateSpeed * Time.deltaTime);
    }

    private RectTransform _rectComponent;
}
