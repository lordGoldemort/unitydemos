﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public DictionaryPanel DictionaryPanel;
    public MainPanel MainPanel;
    public Camera Camera;
    public float DictionaryZ;
    public float MainWindowZ;
    public float TweenTime;
   
    // Start is called before the first frame update
    void Start()
    {
        _targetZ = MainWindowZ;        
    }

    // Update is called once per frame
    void Update()
    {
        var cameraPos = Mathf.SmoothDamp(Camera.transform.position.z, _targetZ, ref _velocity, TweenTime);
        Camera.transform.position = new Vector3(0f, 0f, cameraPos);
    }

    public void OnSummonDictionary()
    {
        _velocity = 0f;
        _targetZ = DictionaryZ;
        DictionaryPanel.Lookup(MainPanel.SelectedText);
    }

    public void OnDismissDictionary()
    {
        _velocity = 0f;
        _targetZ = MainWindowZ;
    }

    public void OnCopyClicked()
    {
        ClipboardHelper.ClipBoard = MainPanel.SelectedText;
        MainPanel.PasteButton.interactable = true;
        MainPanel.SetStatusText("Selection copied to clipboard");
    }

    public void OnPasteClicked()
    {
        var oldText = MainPanel.UserText.text;
        var newText = oldText.Substring(0, MainPanel.UserText.caretPosition) + 
                      ClipboardHelper.ClipBoard +
                      oldText.Substring(MainPanel.UserText.caretPosition);
        MainPanel.UserText.text = newText;
    }

    public void OnCutClicked()
    {
        ClipboardHelper.ClipBoard = MainPanel.SelectedText;
        MainPanel.PasteButton.interactable = true;
        MainPanel.SetStatusText("Selection copied to clipboard");
        MainPanel.CutSelection();
    }
    
    private float _targetZ;
    private float _velocity;
}
