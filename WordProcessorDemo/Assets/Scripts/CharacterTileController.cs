﻿using TMPro;
using UnityEngine;

public class CharacterTileController : PooledObject
{
    public float speed;
    public float spinRate;
    public float xyCone;
    public TMP_Text text;

    private void Awake()
    {
    }
    
    private void OnEnable()
    {        
        var canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;
        
        _spin.x = Random.Range(-1f, 1f) * spinRate;
        _spin.y = Random.Range(-1f, 1f) * spinRate;
        _spin.z = Random.Range(-1f, 1f) * spinRate;

        _velocity.x = Random.Range(-xyCone, xyCone);
        _velocity.y = Random.Range(-xyCone, xyCone);
        _velocity.z = -Random.Range(0.75f, 2.0f) * speed;
    }

    public void SetSymbol(char symbol)
    {
        text.text = symbol.ToString();        
    }
    
    // Update is called once per frame
    private void Update()
    {
        transform.Rotate(_spin*Time.deltaTime);
        transform.position += _velocity * Time.deltaTime;
        if (transform.position.z < Camera.main.transform.position.z) {
            Dispose();
        }
    }
    
    private Vector3 _spin;
    private Vector3 _velocity;
}
