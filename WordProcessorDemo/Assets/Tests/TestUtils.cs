﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestUtils
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestUtilsSimplePasses()
        {
            // Horrid hack, but alternatives?
            var jsonPath = TestContext.CurrentContext.TestDirectory + "/../../Assets/Tests/oed_world.json";
            var json = File.ReadAllText(jsonPath);
            var result = DemoUtils.ParseResponse(json);
            Assert.AreEqual("earth, together with all of its countries and peoples", result);
        }
    }
}


